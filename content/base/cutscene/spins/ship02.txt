#(Load font)
FONT 0 base/fonts/starcon.fon

#(... and BG)
TBC 000000

#(Load movie)
ANI1X base/cutscene/spins/ship02.ani

#(Load movie)
ANI2X addons/hires2x/cutscene/spins/ship02.ani

#(Load movie)
ANI4X addons/hires4x/cutscene/spins/ship02.ani

#(Take up entire screen)
DIMS 320 240

#(Fade out)
FTB 250

#(Draw first frame)
DRAW 0

#(Draw first frame)
DRAW 1

#(Fade in)
FTC 250

#(Wait for five minutes, or keypress)
WAIT 300000
